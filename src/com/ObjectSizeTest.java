package com;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

public class ObjectSizeTest {
	private static final int NUMBER_OF_ELEMENTS = 64;
	private static final int NUMBER_OF_ITERATIONS = 1000;


	private static final String HASHMAP_SIZE_REPORT_NAME = "HashMapSize";
	private static final String TREEMAP_SIZE_REPORT_NAME = "TreeMapSize";
	private static final String HASHMAP_SPEED_REPORT_NAME = "HashMapSpeed";
	private static final String TREEMAP_SPEED_REPORT_NAME = "TreeMapSpeed";

	private List<List> list = new ArrayList<>();
	private HashMap<Integer, Integer> hashMap = new HashMap<>();
	private TreeMap<Integer, Integer> treeMap = new TreeMap<>();

	private String getRandomString() {
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder random = new StringBuilder();
		Random rnd = new Random();
		while (random.length() < 18) {
			int index = (int) (rnd.nextFloat() * chars.length());
			random.append(chars.charAt(index));
		}
		return random.toString();
	}

	private File createNewReportFile(String fileName) throws IOException {
		File file = new File("C:\\Users\\Medius\\Desktop\\" + fileName + ".txt");

		boolean created = file.createNewFile();
		if (created) {
			System.out.println("Report " + fileName + ".txt has been created.");
		} else {
			file.delete();
			file.createNewFile();
			System.out.println("File " + fileName + ".txt already existed. Updated.");
		}

		return file;
	}

	private BufferedWriter createReportWriter(String reportName) throws IOException {
		File path = createNewReportFile(reportName);
		return new BufferedWriter(new FileWriter(path));
	}

	private void determHashMapObjectSize() throws IOException, IllegalAccessException, NoSuchFieldException {
		BufferedWriter writer = createReportWriter(HASHMAP_SIZE_REPORT_NAME);
		List<String> fieldNames = Arrays.asList("size", "modCount", "threshold", "loadFactor");
		Class hashMapClass = HashMap.class;
		Long objectSize = 0L;
		Field field;

		for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
			hashMap.put(i, i);
			for (String fieldName : fieldNames) {
				field = hashMapClass.getDeclaredField(fieldName);
				field.setAccessible(true);
				objectSize += InstrumentationAgent.getObjectSize(field.get(hashMap));
			}

			field = hashMapClass.getDeclaredField("table");
			field.setAccessible(true);
			objectSize += InstrumentationAgent.getObjectSize(field.get(hashMap));
			Object[] table = (Object[]) field.get(hashMap);
			for (Object object : table) {
				if (object != null) {
					objectSize += InstrumentationAgent.getObjectSize(object);
				}
			}

			writer.write(objectSize.toString());
			writer.newLine();
			objectSize = 0L;
		}
		writer.close();
		hashMap.clear();
	}

	private void determTreeMapObjectSize() throws IOException, NoSuchFieldException, IllegalAccessException {
		BufferedWriter writer = createReportWriter(TREEMAP_SIZE_REPORT_NAME);
		List<String> fieldNames = Arrays.asList("size", "modCount");
		Class treeMapClass = TreeMap.class;
		Long objectSize = 0L;
		Field field;

		for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
			treeMap.put(i, i);
			for (String fieldName : fieldNames) {
				field = treeMapClass.getDeclaredField(fieldName);
				field.setAccessible(true);
				objectSize += InstrumentationAgent.getObjectSize(field.get(treeMap));
			}

			field = treeMapClass.getDeclaredField("root");
			field.setAccessible(true);
			Object rootElement = field.get(treeMap);
			objectSize = postOrderTreeTraversal(rootElement, objectSize);
			writer.write(objectSize.toString());
			writer.newLine();
			objectSize = 0L;
		}
		writer.close();
		treeMap.clear();
	}

	private Long postOrderTreeTraversal(Object node, Long objectSize)
			throws NoSuchFieldException, IllegalAccessException {
		Class nodeClass = node.getClass();
		Field left = nodeClass.getDeclaredField("left");
		Field right = nodeClass.getDeclaredField("right");
		left.setAccessible(true);
		right.setAccessible(true);
		if (left.get(node) != null) {
			objectSize = postOrderTreeTraversal(left.get(node), objectSize);
		}
		if (right.get(node) != null) {
			objectSize = postOrderTreeTraversal(right.get(node), objectSize);
		}
		return objectSize + InstrumentationAgent.getObjectSize(node);
	}

	private void determMapMethodsSpeed(Map<Integer, Integer> map, String reportName) throws IOException {
		BufferedWriter writer = createReportWriter(reportName);
		writer.write("Number of iterations " + NUMBER_OF_ITERATIONS);
		writer.newLine();

		writer.write("Put test. Cold JRE time " + putTimeMeasure(map) + " ns");
		writer.newLine();

		writer.write("Put test. Warmed JRE time " + putTimeMeasure(map) + " ns");
		writer.newLine();

		writer.write("Get test. Cold JRE time " + getTimeMeasure(map) + " ns");
		writer.newLine();

		writer.write("Get test. Warmed JRE time " + getTimeMeasure(map) + " ns");
		writer.newLine();

		writer.write("Remove test. Cold JRE time " + removeTimeMeasure(map) + " ns");
		writer.newLine();

		writer.write("Remove test. Warmed JRE time " + removeTimeMeasure(map) + " ns");
		writer.newLine();

		writer.close();
		map.clear();
	}

	private long putTimeMeasure(Map<Integer, Integer> map) {
		long start = System.nanoTime();
		for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
			map.put(i, i);
		}
		long end = System.nanoTime();
		return end - start;
	}

	private long getTimeMeasure(Map<Integer, Integer> map) {
		long start = System.nanoTime();
		for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
			map.get(i);
		}
		long end = System.nanoTime();
		return end - start;
	}

	private long removeTimeMeasure(Map<Integer, Integer> map) {
		long start = System.nanoTime();
		for (int i = 0; i < NUMBER_OF_ITERATIONS; i++) {
			map.remove(i);
		}
		long end = System.nanoTime();
		return end - start;
	}

	private void start() throws NoSuchFieldException, IllegalAccessException, IOException {
		determHashMapObjectSize();
		determTreeMapObjectSize();
		determMapMethodsSpeed(hashMap, HASHMAP_SPEED_REPORT_NAME);
		determMapMethodsSpeed(treeMap, TREEMAP_SPEED_REPORT_NAME);
	}

	public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, IOException {
		new ObjectSizeTest().start();
	}
}
